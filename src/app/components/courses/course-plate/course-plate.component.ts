import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-course-plate',
  templateUrl: './course-plate.component.html',
  styleUrls: ['./course-plate.component.scss'],
})
export class CoursePlateComponent implements OnInit {

  @Input() public course: any;
  @Output() deleteCourse = new EventEmitter<number>();

  constructor() { }

  ngOnInit() {

  }

  _deleteCourse(id: number) {
    this.deleteCourse.emit(id);
  }

}
