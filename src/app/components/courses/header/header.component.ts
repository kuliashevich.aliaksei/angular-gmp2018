import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {

  @Output() logOff = new EventEmitter<string>();

  username = 'User';

  constructor() { }

  ngOnInit() {
  }

  _logOff(name: string) {
    this.logOff.emit(name);
  }

}
