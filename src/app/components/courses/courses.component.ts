import { Component, OnInit } from '@angular/core';
import { CoursesService } from '../../services/courses.service';
@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.scss'],
})
export class CoursesComponent implements OnInit {

  courses: any = [];

  deleteCourse(id) {

    console.log('-------id:', id);
  }

  logOff(name) {

    console.log('-------name:', name);
  }

  loadMore(e) {

    console.log('-------loadMode:', e);
  }

  addCourse(e) {

    console.log('-------addCourse:', e);
  }

  constructor( private coursesService: CoursesService ) { }

  ngOnInit() {

    this.courses = this.coursesService.getData();
  }

}
