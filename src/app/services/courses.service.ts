import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class CoursesService {

  courses = [
    {
      id: 1,
      title: 'Video Course',
      duration: '1h 28min',
      creationDate: '05.09.2018',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco labor nisi ut aliquip ex ea commodo consequat.',
   },
   {
      id: 2,
      title: 'Video Course2',
      duration: '1h 08min',
      creationDate: '08.12.2018',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco labor nisi ut aliquip ex ea commodo consequat.',
  },
  ];

  getData() {

    return this.courses;
  }

  addData(course: any) {

    this.courses.push(course);
  }

  constructor() { }
}
