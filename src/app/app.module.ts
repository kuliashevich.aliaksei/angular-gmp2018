import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { CoursesComponent } from './components/courses/courses.component';
import { HeaderComponent } from './components/courses/header/header.component';
import { LogoComponent } from './components/courses/logo/logo.component';
import { FooterComponent } from './components/courses/footer/footer.component';
import { BreadcrumbsComponent } from './components/courses/breadcrumbs/breadcrumbs.component';
import { SearchControlComponent } from './components/courses/search-control/search-control.component';
import { CoursePlateComponent } from './components/courses/course-plate/course-plate.component';
import { CoursesService } from './services/courses.service';

@NgModule({
  declarations: [
    CoursesComponent,
    HeaderComponent,
    LogoComponent,
    FooterComponent,
    BreadcrumbsComponent,
    SearchControlComponent,
    CoursePlateComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
  ],
  providers: [CoursesService],
  bootstrap: [CoursesComponent],
})
export class AppModule { }
