export interface Course {
  id: number;
  title: string;
  creationDate: number;
  duration: any;
  description: string;
}
